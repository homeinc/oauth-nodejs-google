const db = require('../db/pg'); 

class User {
  constructor(id, name, email) {
    this.id = id;
    this.name = name;
    this.email = email;
  }

  static async getById(id) {
    return db.query('SELECT * FROM users WHERE id = $1 LIMIT 1', [id])
      .then((data) => new User(data.id, data.name, data.email))
      .catch((error) => {
        console.error('Error getting user by ID:', error);
        throw error;
      });
  }

  static async getByEmail(email) {
    db.query('SELECT * FROM users WHERE email = $1 LIMIT 1', [email], (err, result) => {
       if (err) {
        console.log("ERROR : ", err);
        return;
       }
        console.log("REZULT = ", result.rows[0])
        return result.rows[0]
    })
    //   .then((data) => { return  data.rows })
    //   .catch((error) => {
    //         console.error('Error getting user by Email:', error);
    //     throw error;
    //   });
  }

  static async getAll() {
    return db.query('SELECT * FROM users')
      .then((data) => data.map((row) => new User(row.id, row.name, row.email)))
      .catch((error) => {
        console.error('Error getting all users:', error);
        throw error;
      });
  }

  async save() {
    return db.query('INSERT INTO users(name, email) VALUES($1, $2)', [this.name, this.email])
      .catch((error) => {
        console.error('Error saving user:', error);
        throw error;
      });
  }

  async update() {
    return db.query('UPDATE users SET name = $1, email = $2 WHERE id = $3', [this.name, this.email, this.id])
      .catch((error) => {
        console.error('Error updating user:', error);
        throw error;
      });
  }

  async delete() {
    return db.query('DELETE FROM users WHERE id = $1', [this.id])
      .catch((error) => {
        console.error('Error deleting user:', error);
        throw error;
      });
  }
}

module.exports = User;
