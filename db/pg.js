const { Pool } = require('pg')
require('dotenv').config()

const pgPool = new Pool({
  host                 : process.env.DB_HOSTNAME,            // Postgres ip address[s] or domain name[s]
  port                 : 5432,          // Postgres server port[s]
  database             : process.env.DB_NAME,            // Name of database to connect to
  user             :     process.env.DB_USER,            // Username of database user
  password             : process.env.DB_PASSWORD,            // Password of database user
})


pgPool.query('SELECT NOW()', (err, res) => {
    if (err) {
      console.error('Error connecting to PostgreSQL:', err);
    } else {
      console.log('Connected to PostgreSQL:', res.rows[0].now);
    }
  });


module.exports=pgPool