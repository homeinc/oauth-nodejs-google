const  GoogleStrategy = require( 'passport-google-oauth20' ).Strategy;
const passport = require('passport')
require('dotenv').config()
const dbPool = require('../db/pg')
const User = require( '../models/User.model')

passport.serializeUser((user, done) => {
    console.log("Serialize ", user)
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    // Look up the user in your database by ID
    dbPool.query("SELECT * FROM users WHERE id= $1", [id], (err, result) => {
        console.log("User = ", result.rows[0])
        return done(err, result.rows[0])
        })
  });

passport.use(new GoogleStrategy({
    clientID:     process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: "http://localhost:3000/auth/google/redirect",
    passReqToCallback   : true
  },
   (request, accessToken, refreshToken, profile, done)=> {
    const {value, verified }  = profile.emails.pop()

    dbPool.query("SELECT * FROM users WHERE email= $1", [value], (err, result) => {
        console.log(result)
        if (result.rowCount ==0) {
            //if user doesn't exist in DB, we have to create it and after, select from DB
            dbPool.query("INSERT into users(email, password) VALUES($1, $2)", 
                         [value, 'someencryptedpassword'], (err, res) => {

                if (!err) {            
                dbPool.query("SELECT * FROM users WHERE email= $1", [value], (err, result) => {
                    
                    done( err, result.rows[0])
                })
            }
            })  
        } else {

            console.log("User = ", result.rows[0])
            done(null, result.rows[0])
        }
    })
   }
))