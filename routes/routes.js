const express = require("express");
const router = express.Router();
const passport = require('passport')
require('../auth/google')

router.get('/', (req, res) => {
    res.render('welcome')
})


router.get('/auth/google',
  passport.authenticate('google', { scope:
      [ 'email', 'profile' ] }
));

router.get('/auth/google/redirect', passport.authenticate('google'), (req, res) => {
    res.send(req.user)
})

module.exports=router