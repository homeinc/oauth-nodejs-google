const express= require('express')
const session = require('express-session');
const app = express()
const routes = require('./routes/routes')
const dbPool = require ('./db/pg')
const passport = require('passport')
const cookie = require('cookie-session')

app.set('view engine', 'ejs');

app.use( (req, res, next) => {
    req.dbPool = dbPool
    next()
})

app.use(
    session({
      secret: process.env.COOKIE_KEY, // Replace with your secret key
      resave: false,
      saveUninitialized: true,
    })
  );

// app.use(cookie({
//     maxAge : 86400* 1000,
//     keys: []
// }))

app.use(passport.initialize());
app.use(passport.session());


app.use('/', routes)





const PORT = 3000
app.listen(PORT, () => {
    console.log("App is running...")
})